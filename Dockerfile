FROM python:3.4-slim-jessie

RUN apt-get update
RUN apt-get install -y g++ gcc python3-dev libxml2-dev libxslt1-dev python3-lxml zlib1g-dev libssl-dev iputils-ping curl wget

RUN rm /etc/localtime
RUN ln -s /usr/share/zoneinfo/Europe/Bucharest /etc/localtime
RUN date

ADD requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
RUN pip3 install uwsgi

COPY ./fifa /var/www/html/fifa18/fifa
WORKDIR /var/www/html/fifa18/fifa
RUN cp ./fifa/localsettings.py.example ./fifa/localsettings.py

ADD fifa18.server.lan.key fifa18.server.lan.key
ADD fifa18serverlan.pem fifa18serverlan.pem
RUN openssl x509 -in fifa18serverlan.pem -out fifa18.server.lan.crt

RUN python manage.py check
RUN python manage.py collectstatic

EXPOSE 443
RUN export LANG='en_US.UTF-8'
RUN export LC_ALL='en_US.UTF-8'
CMD [ "uwsgi", "--https", ":443,fifa18.server.lan.crt,fifa18.server.lan.key", \
               "--http-to-https", ":80", \
               "--enable-threads", \
               "--static-map", "/static=/var/www/html/fifa18/static", \
               "--wsgi", "fifa.wsgi" ]
