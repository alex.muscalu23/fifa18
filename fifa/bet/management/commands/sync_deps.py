import csv

from django.contrib.auth.models import User
from django.core.management import BaseCommand
from django.db import transaction

from bet.models import UserInfo


class Command(BaseCommand):

    def handle(self, *args, **options):
        with open('data.csv', 'r') as file:
            reader = csv.DictReader(file, fieldnames=['user', 'department_name', 'location_name'])
            with transaction.atomic():
                for row in reader:
                    user = User.objects.get(username=row['user'])
                    print("Parsing " + user.username)
                    try:
                        user.info
                    except UserInfo.DoesNotExist:
                        UserInfo.objects.create(user=user, department_name=row['department_name'], location_name=row['location_name'])
                        print("User info added for " + user.username)
