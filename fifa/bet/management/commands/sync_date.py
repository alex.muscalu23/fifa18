import requests
from dateutil import parser
from django.core.management import BaseCommand
from pyquery import PyQuery as pq

from bet.models import Country, Match


class Command(BaseCommand):

    def handle(self, *args, **options):
        matches = Match.objects.all()
        for match in matches:
            if match.fifa_url:
                response = requests.get(match.fifa_url)
                dom = pq(response.text)
                date_string = dom('.fi-mu__info__datetime').text()
                match.time = parser.parse(date_string.replace(' Local time', ''))
                match.save()
