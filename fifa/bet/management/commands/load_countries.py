import csv

from django.core.management import BaseCommand

from bet.models import Country


class Command(BaseCommand):

    def handle(self, *args, **options):
        with open('countries.csv') as csv_file:
            for row in csv.DictReader(csv_file, delimiter=','):
                existing = Country.objects.filter(name=row['name'])
                if existing.count():
                    continue
                Country.objects.create(
                    name=row['name'],
                    img_src=row['img_src']
                )
