import csv
from datetime import timedelta

from dateutil import parser
from django.core.management import BaseCommand

from bet.models import Country, Match


class Command(BaseCommand):
    def handle(self, *args, **options):
        with open('matches.csv') as csv_file:
            for row in csv.DictReader(csv_file, delimiter=','):
                home = Country.objects.get(name=row['home'].replace(" ", ""))
                away = Country.objects.get(name=row['away'].replace(" ", ""))
                existing = Match.objects.filter(home=home, away=away, stage=row['stage'])
                if existing.count():
                    existing[0].time = parser.parse(row['time']) + timedelta(hours=3)
                    existing[0].fifa_url = row['url']
                    existing[0].save()
                    continue
                Match.objects.create(
                    home=home,
                    away=away,
                    stage=row['stage'],
                    fifa_url=row['url'],
                    time=parser.parse(row['time']) + timedelta(hours=3)
                )
