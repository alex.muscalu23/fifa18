from datetime import datetime
from urllib.parse import unquote

from django.conf import settings
from django.http import Http404
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import CreateView
from django.views.generic import TemplateView
from django.contrib.auth import authenticate, logout, login
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User

from bet.models import Match, Bet, SuperBet, Country, UserInfo


class LoginRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return super().dispatch(request, *args, **kwargs)
        return redirect('login')


class LoginView(TemplateView):
    template_name = 'login.html'

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect('home')
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        if 'ticket' not in request.GET:
            return super().get(request, *args, **kwargs)
        ticket = request.GET['ticket']
        user = authenticate(request, ticket=ticket)
        if user:
            login(request, user)
            return redirect('home')
        return redirect('login')

    def post(self, request, *args, **kwargs):
        return redirect(settings.CAS_LOGIN_URL.format(settings.CURRENT_SERVICE))


def logout_view(request):
    logout(request)
    return redirect('login')


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = 'home.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        matches = Match.objects.filter(time__date=datetime.now().date()).order_by('time').all()
        if matches.exists():
            context['matches'] = matches
        else:
            next_match = Match.objects.filter(time__gte=datetime.now()).order_by('time').first()
            if next_match:
                context['next_match'] = next_match
        return context


class MatchesView(LoginRequiredMixin, TemplateView):
    template_name = 'matches/matches.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['matches'] = Match.objects.filter(time__gt=datetime.now()).order_by('time').prefetch_related('away', 'home').all()
        context['bets'] = self.request.user.bets.values_list('match__pk', flat=True)

        return context


class BetsView(LoginRequiredMixin, TemplateView):
    template_name = 'bets/my_bets.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        bets = Bet.objects.filter(user=self.request.user).prefetch_related('match', 'match__away', 'match__home').order_by('-match__time')

        completed = []
        in_progress = []

        for bet in bets:
            if bet.match.is_frozen():
                completed.append(bet)
            else:
                in_progress.insert(0, bet)

        context['completed_bets'] = completed
        context['in_progress_bets'] = in_progress
        try:
            user = self.request.user
            context['super_bet'] = user.super_bet
        except SuperBet.DoesNotExist:
            pass
        context['can_super_bet'] = SuperBet.can_be_saved()
        return context


class LeaderboardView(LoginRequiredMixin, TemplateView):
    template_name = 'leaderboard.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        users = User.objects.prefetch_related('info').all()
        result = {}
        for user in users:
            full_name = user.get_full_name()
            result.update({
                full_name: {
                    'score': 0,
                    'super_won': 0,
                    'won': 0,
                    'placed': 0,
                    'department': user.info.department_name
                }
            })
        bets = Bet.objects.prefetch_related('match', 'user').all()
        for bet in bets:
            user = bet.user.get_full_name()
            result[user]['score'] += bet.get_score()
            result[user]['placed'] += 1
            if bet.is_exact:
                result[user]['super_won'] += 1
            elif bet.is_correct:
                result[user]['won'] += 1
        super_bets = SuperBet.objects.prefetch_related('user', 'country').all()
        match = SuperBet.get_match()
        for super_bet in super_bets:
            user = super_bet.user.get_full_name()
            result[user]['score'] += super_bet.get_score(match)
            result[user]['super_bet'] = super_bet

        scores = [result[key]['score'] for key in result.keys()]
        scores = list(set(scores))
        scores.sort(reverse=True)
        for user in result.keys():
            result[user]['position'] = scores.index(result[user]['score']) + 1
        context['result'] = result
        return context


class UserDepartmentLeaderboardView(LoginRequiredMixin, TemplateView):
    template_name = 'leaderboard.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        users = User.objects.filter(info__department_name=unquote(self.kwargs['d'])).all()
        result = {}
        for user in users:
            full_name = user.first_name + ' ' + user.last_name
            result.update({
                full_name: {
                    'score': 0,
                    'super_won': 0,
                    'won': 0,
                    'placed': 0
                }
            })
        bets = Bet.objects.filter(user__in=users).prefetch_related('match', 'user', 'user__info').all()
        for bet in bets:
            user = bet.user.get_full_name()
            result[user]['score'] += bet.get_score()
            result[user]['placed'] += 1
            if bet.is_exact:
                result[user]['super_won'] += 1
            elif bet.is_correct:
                result[user]['won'] += 1
        super_bets = SuperBet.objects.filter(user__in=users).prefetch_related('user', 'country').all()
        match = SuperBet.get_match()
        for super_bet in super_bets:
            user = super_bet.user.get_full_name()
            result[user]['score'] += super_bet.get_score(match)
            result[user]['super_bet'] = super_bet

        scores = [result[key]['score'] for key in result.keys()]
        scores = list(set(scores))
        scores.sort(reverse=True)
        for user in result.keys():
            result[user]['position'] = scores.index(result[user]['score']) + 1
        context['result'] = result
        return context


class DepartmentLeaderboardView(LoginRequiredMixin, TemplateView):
    template_name = 'leaderboard.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        infos = UserInfo.objects.all()
        result = {}
        for info in infos:
            department = info.department_name
            if department not in result:
                result.update({
                    department: {
                        'score': 0,
                        'super_won': 0,
                        'won': 0,
                        'members': 1
                    }
                })
            else:
                result[department]['members'] += 1
        bets = Bet.objects.filter(user__info__isnull=False).prefetch_related('match', 'user')\
            .select_related('user__info').all()
        for bet in bets:
            department = bet.user.info.department_name
            result[department]['score'] += bet.get_score()
            if bet.is_exact:
                result[department]['super_won'] += 1
            elif bet.is_correct:
                result[department]['won'] += 1
        super_bets = SuperBet.objects.filter(user__info__isnull=False).prefetch_related('user', 'country')\
            .select_related('user__info').all()
        match = SuperBet.get_match()
        for super_bet in super_bets:
            department = super_bet.user.info.department_name
            result[department]['score'] += super_bet.get_score(match)

        scores = [result[key]['score'] for key in result.keys()]
        scores = list(set(scores))
        scores.sort(reverse=True)
        for department in result.keys():
            result[department]['position'] = scores.index(result[department]['score']) + 1
            result[department]['placed'] = int(result[department]['score'] / result[department]['members'])
        context['result'] = result
        return context


class LocationLeaderboardView(LoginRequiredMixin, TemplateView):
    template_name = 'leaderboard.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        infos = UserInfo.objects.all()
        result = {}
        for info in infos:
            location = info.location_name
            result.update({
                location: {
                    'score': 0,
                    'super_won': 0,
                    'won': 0,
                    'placed': 0
                }
            })
        bets = Bet.objects.filter(user__info__isnull=False).prefetch_related('match', 'user', 'user__info').all()
        for bet in bets:
            location = bet.user.info.location_name
            result[location]['score'] += bet.get_score()
            result[location]['placed'] += 1
            if bet.is_exact:
                result[location]['super_won'] += 1
            elif bet.is_correct:
                result[location]['won'] += 1
        super_bets = SuperBet.objects.filter(user__info__isnull=False).prefetch_related('user', 'country') \
            .select_related('user__info').all()
        match = SuperBet.get_match()
        for super_bet in super_bets:
            location = super_bet.user.info.location_name
            result[location]['score'] += super_bet.get_score(match)

        scores = [result[key]['score'] for key in result.keys()]
        scores = list(set(scores))
        scores.sort(reverse=True)
        for location in result.keys():
            result[location]['position'] = scores.index(result[location]['score']) + 1
        context['result'] = result
        return context


class BetCreate(CreateView):
    template_name = 'bets/add_bet.html'
    model = Bet
    fields = ['home_goals', 'away_goals']

    def get_success_url(self):
        return reverse('my_bets')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['match'] = Match.objects.prefetch_related('away', 'home').get(pk=self.kwargs['pk'])
        existing = Bet.objects.filter(user=self.request.user,
                                      match=context['match']).all()
        if existing.exists():
            context['bet'] = existing.first()
        return context

    def form_valid(self, form):
        try:
            match = Match.objects.get(pk=self.kwargs['pk'], time__gt=datetime.now())
        except Match.DoesNotExist:
            raise Http404('Match does not exist or is already started')

        existing = Bet.objects.filter(user=self.request.user, match=match).all()
        if existing.exists():
            existing.delete()
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.match = match
        obj.save()
        return HttpResponseRedirect(self.get_success_url())


class SuperBetCreate(CreateView):
    template_name = 'bets/add_super_bet.html'
    model = SuperBet
    fields = ['country']

    def get_success_url(self):
        return reverse('my_bets')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['countries'] = Country.objects.order_by('name').all()
        try:
            user = self.request.user
            context['super_bet'] = user.super_bet
        except SuperBet.DoesNotExist:
            pass
        return context

    def form_valid(self, form):
        if not SuperBet.can_be_saved():
            raise Http404("Final already started")
        existing = SuperBet.objects.filter(user=self.request.user).all()
        if existing.exists():
            if existing.first().country != form.cleaned_data['country']:
                existing.delete()
            else:
                return HttpResponseRedirect(self.get_success_url())
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.save()
        return HttpResponseRedirect(self.get_success_url())


class MatchResultsView(LoginRequiredMixin, TemplateView):
    template_name = 'matches/matches_results.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['matches'] = Match.objects.filter(time__lt=datetime.now())\
            .order_by('-time').prefetch_related('home', 'away').all()
        return context


class AllBetsView(LoginRequiredMixin, TemplateView):
    template_name = 'bets/all_bets.html'

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        try:
            match = Match.objects.get(pk=self.kwargs['pk'], time__lt=datetime.now())
        except Match.DoesNotExist:
            raise Http404('Match does not exist or is not started')
        context['match'] = match
        context['bets'] = Bet.objects.filter(match=match)\
            .prefetch_related('match', 'match__away', 'match__home', 'user').all()
        return context
