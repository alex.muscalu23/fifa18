import datetime

import pytz
from django.db import models
from django.db.models import Model
from django.contrib.auth.models import User


class Country(Model):
    name = models.CharField(max_length=20)
    img_src = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Countries'

    def __str__(self):
        return self.name


class Match(Model):
    STAGES = (
        ('GROUP', 'GROUP'),
        ('ROUND OF 16', 'ROUND OF 16'),
        ('QUARTER-FINALS', 'QUARTER-FINALS'),
        ('SEMI-FINALS', 'SEMI-FINALS'),
        ('THIRD PLACE', 'THIRD PLACE'),
        ('FINALS', 'FINALS'),
    )
    home = models.ForeignKey(Country, related_name='home_matches', on_delete=models.CASCADE)
    away = models.ForeignKey(Country, related_name='away_matches', on_delete=models.CASCADE)
    time = models.DateTimeField()
    stage = models.CharField(max_length=45, choices=STAGES)
    home_goals = models.PositiveIntegerField(blank=True, null=True)
    away_goals = models.PositiveIntegerField(blank=True, null=True)
    home_penalties = models.PositiveIntegerField(blank=True, null=True)
    away_penalties = models.PositiveIntegerField(blank=True, null=True)
    fifa_url = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return "{} vs {}".format(self.home.name, self.away.name)

    def is_frozen(self):
        return self.time < datetime.datetime.now(pytz.utc)

    def has_bet_on(self, user):
        return self.bets.filter(user=user).exists()

    class Meta:
        verbose_name_plural = 'Matches'


class Bet(Model):
    user = models.ForeignKey(User, related_name='bets', on_delete=models.CASCADE)
    match = models.ForeignKey(Match, related_name='bets', on_delete=models.CASCADE)
    home_goals = models.PositiveIntegerField(default=0)
    away_goals = models.PositiveIntegerField(default=0)

    def __str__(self):
        return "{} - {} : {} - {}".format(self.match, self.home_goals, self.away_goals, self.user)

    @property
    def is_exact(self):
        if self.match.home_goals is None or self.match.away_goals is None:
            return False
        return self.match.home_goals == self.home_goals and self.match.away_goals == self.away_goals

    @property
    def is_correct(self):
        if self.match.home_goals is None or self.match.away_goals is None:
            return False
        sign = lambda x: x and (1, -1)[x < 0]
        return (
            sign(self.match.home_goals - self.match.away_goals)
            ==
            sign(self.home_goals - self.away_goals)
        )

    def get_score(self):
        if self.is_exact:
            return 3
        if self.is_correct:
            return 2
        return 0


class SuperBet(Model):
    user = models.OneToOneField(User, related_name='super_bet', on_delete=models.CASCADE)
    country = models.ForeignKey(Country, related_name='super_bets', on_delete=models.CASCADE)
    last_updated = models.DateTimeField(auto_now=True)

    @staticmethod
    def get_match():
        match = Match.objects.filter(stage='FINALS')
        if not match.exists():
            return None
        return match.first()

    @staticmethod
    def can_be_saved():
        match = Match.objects.filter(stage='FINALS')
        if not match.exists():
            return True
        match = match.first()
        return not match.is_frozen()

    def won(self, match):
        if not match:
            return
        if match.home_goals is None or match.away_goals is None:
            return
        if match.home_goals > match.away_goals:
            return match.home == self.country
        elif match.away_goals > match.home_goals:
            return match.away == self.country
        elif match.home_penalties > match.away_penalties:
            return match.home == self.country
        return match.away == self.country

    def get_score(self, match):
        if self.won(match) is not None and self.won(match):
            return 2 + round(0.33 * (match.time - self.last_updated).days)
        return 0

    def __str__(self):
        return str(self.country)


class UserInfo(models.Model):
    user = models.OneToOneField("auth.User", related_name='info', on_delete=models.CASCADE)

    department_name = models.CharField(max_length=512)
    location_name = models.CharField(max_length=512)

    def __str__(self):
        return self.user.username + " - " + self.department_name
