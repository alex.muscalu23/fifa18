from urllib.parse import quote

from django import template
from django.conf import settings
from tz_detect.utils import convert_header_name

register = template.Library()


def has_bet_on(match, user):
    return match.has_bet_on(user)


register.filter('has_bet_on', has_bet_on)


def safe_urlencode(data):
    return quote(data, safe='')


register.filter('safe_urlencode', safe_urlencode)


@register.inclusion_tag('detector.html', takes_context=True)
def tz_detect(context):
    return {
        'show': True,
        'debug': getattr(settings, 'DEBUG', False),
        'csrf_header_name': convert_header_name(
            getattr(settings, 'CSRF_HEADER_NAME', 'HTTP_X_CSRFTOKEN')
        ),
    }
