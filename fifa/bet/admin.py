from django.contrib import admin

from bet.models import Match, Bet, Country, SuperBet, UserInfo


class MatchAdmin(admin.ModelAdmin):
    list_display = ('home', 'away', 'home_goals', 'away_goals')
    list_filter = ('home', 'away')


class BetAdmin(admin.ModelAdmin):
    search_fields = ('user__username',)


admin.site.register(Match, MatchAdmin)
admin.site.register(Bet, BetAdmin)
admin.site.register(SuperBet)
admin.site.register(Country)
admin.site.register(UserInfo)
