# Generated by Django 2.0.6 on 2018-06-11 16:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bet', '0006_auto_20180611_1847'),
    ]

    operations = [
        migrations.AlterField(
            model_name='superbet',
            name='last_updated',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
