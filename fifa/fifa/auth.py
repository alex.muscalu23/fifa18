# -*- coding: utf-8 -*-
import logging
from django.conf import settings
from django.contrib.auth.models import User
from xmltodict import parse

import requests

from bet.models import UserInfo

logger = logging.getLogger()


class CASBackend:
    def authenticate(self, request, ticket=None):
        if not ticket:
            logger.error("No ticket received")
            return None
        cas_response = requests.get(settings.CAS_VALIDATE_URL.format(
            settings.CURRENT_SERVICE, ticket))
        tree = parse(cas_response.content)
        if not tree.get('cas:serviceResponse', False):
            logger.error("No CAS response")
            return None
        service_response = tree.get('cas:serviceResponse')
        if (not service_response.get('cas:authenticationSuccess', False) and
                not service_response.get('cas:authenticationFailure', False)):
            logger.error("CAS response not valid")
            return None
        if service_response.get('cas:authenticationFailure', False):
            logger.error("CAS authentication failed" + str(service_response.get('cas:authenticationFailure')))
            return None
        success = service_response.get('cas:authenticationSuccess', False)
        if not success.get('cas:user', False):
            logger.error("No CAS user found" + success)
            return None

        username = success.get('cas:user')
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            attributes = success.get('cas:attributes')
            last_name = ''
            if 'cas:lastName' in attributes:
                last_name = attributes['cas:lastName']
            first_name = ''
            if 'cas:firstName' in attributes:
                first_name = attributes['cas:firstName']
            email = ''
            if 'cas:email' in attributes:
                email = attributes['cas:email']
            User.objects.create(username=username,
                                email=email,
                                first_name=first_name,
                                last_name=last_name)
        user = User.objects.get(username=username)
        try:
            UserInfo.objects.get(user=user)
        except UserInfo.DoesNotExist:
            attributes = success.get('cas:attributes')
            if 'cas:email' in attributes:
                user.email = attributes['cas:email']
                user.save()
            UserInfo.objects.create(
                user=user,
                department_name=attributes.get('cas:departmentName', ''),
                location_name=attributes.get('cas:locationName', '')
            )
        return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
